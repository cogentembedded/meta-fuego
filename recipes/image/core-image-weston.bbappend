IMAGE_INSTALL_append = " \
	strace \
	usbutils \
	i2c-tools \
	alsa-utils \
	devmem2 \
	iw \
	hdparm \
	rt-tests \
	fb-test \
	evtest \
	bc \
	connman-tests \
	procps \
	util-linux \
	coreutils \
	wget \
	rsync \
	e2fsprogs-mke2fs \
	dosfstools \
	parted \
	perl \
	perl-modules \
	libpcre \
	bzip2 \
	libpng \
	bash \
	ldd \
	libaio \
	python-core python-modules \
        alsa-lib \
        gettext \
        eglibc-staticdev \
"


TOOLCHAIN_TARGET_TASK_append = " eglibc-staticdev alsa-dev libaio-dev"

LICENSE = "MIT"


